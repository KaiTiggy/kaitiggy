import React from 'react'
import ReactDOM from 'react-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import './index.css'
import Router from './utils/Router.js'

ReactDOM.render(<Router />, document.getElementById('root'))
